# s3-sha256


## Intro

The objective of this small repo is to demonstrate how to use S3 to generate SHA256 hashes. 

It contains a small python script that uploads a file to S3 and checks the generated checksum.


## Installing python boto3

Follow quickstart from:
https://boto3.amazonaws.com/v1/documentation/api/latest/guide/quickstart.html

```
pip install 'boto3[crt]'
```

## Testing the script
Once the boto3 library + CRT extension are installed, test the python script with a command similar to:
```
python3 upload_file.py -b <bucket> -f <file>
```



## Pre-signed URLs

### Generating the pre-signed URL
S3 doesn't support checksums + presigned URLs if the checksum is not known upfront when the presigned URL is generated.

If the checksum is known upfront a code similar to this one can be used to upload the file:
```python
import boto3
import requests


data = b'foobar2'
chksum = 'vptMlnSuOTLPOCs2JFjWEH6UM9HZ4FvvPu8Ye7d4XAU='


s3 = boto3.client('s3')
url = s3.generate_presigned_url(
    'put_object',
    Params={
        'Bucket': 'bucketname',
        'Key': 'somekey',
        'ChecksumSHA256': chksum
    },
)

headers = {'x-amz-checksum-sha256': chksum}
response = requests.put(url, data=data, headers=headers)
print(response.status_code, response.content)
```

Note: this code uses the `put_object` function and not the `upload_file` function that we use in the script.
Refer to the documentation for more details: https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3/client/put_object.html



### Testing the pre-signed URL
To test the pre-signed URL, a curl command similar to this one can be used: 

```
curl -X PUT -T <objectname> -H 'x-amz-checksum-sha256: <SHA256-HASH>' <Pre-signed URL from 'response'>
```


## Manually checking checksums

Checksums can also be checked manually with commands similar to:
shasum -a 256 image.jpg | cut -f1 -d\ | xxd -r -p | base64



import argparse,boto3,hashlib,base64

def compute_sha256(file):

  with open(file,"rb") as f:
    file_content = f.read() 
    sha256_hash = hashlib.sha256(file_content) 
    print("SHA256 hash in hex:", sha256_hash.hexdigest())

    sha256_base64 = base64.b64encode(sha256_hash.digest()).decode("utf-8")
    return sha256_base64

   
def upload_file(bucket, file):

  print("First computing hash")
  sha256_base64_hash = compute_sha256(file)
  print("Base64 encoded hash:")
  print(sha256_base64_hash)

  print("Uploading file to S3...")
  s3=boto3.client('s3')
  s3.upload_file(file,bucket,file,ExtraArgs={'ChecksumAlgorithm': 'SHA256'})

  print("Hash computed by S3:")
  json_answer = s3.get_object_attributes(Bucket=bucket,Key=file,ObjectAttributes=['Checksum'])
  checksum = json_answer['Checksum']['ChecksumSHA256']
  print(checksum)

  if checksum == sha256_base64_hash:
    print("Checksums match!")
  else:
    print("Checksums do not match! Please investigate.") 
  

if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='upload_file.py',
                                     description='Python script showing how to upload a file to S3 and check its SHA256 hash')
    parser.add_argument('-b', '--bucket', help='Name of the bucket where the file should be uploaded', required=True)
    parser.add_argument('-f', '--file', help='Name of the file to be uploaded', required=True)
    args = parser.parse_args()
    upload_file(args.bucket,args.file)

